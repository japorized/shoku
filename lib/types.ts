export enum Color {
  BLACK,
  RED,
  GREEN,
  YELLOW,
  BLUE,
  MAGENTA,
  CYAN,
  GRAY,
  DARKGRAY,
  BRIGHTRED,
  BRIGHTGREEN,
  BRIGHTYELLOW,
  BRIGHTBLUE,
  BRIGHTMAGENTA,
  BRIGHTCYAN,
  WHITE,
  NONE,
}

export enum Formatting {
  BOLD = "bold",
  DIM = "dim",
  UNDERLINE = "smul",
  REVERSE = "rev",
}

export type HighlightOptions = {
  color: Color;
  background: Color;
  formatting: Array<Formatting>;
};

export type Stringable = string | number;
