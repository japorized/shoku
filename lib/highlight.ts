import getTputEscapeCode from "./getTputEscapeCode.ts";
import { HighlightOptions, Color, Stringable } from "./types.ts";

// Reference for tput:
// http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/x405.html

/**
 * @function generic highlight module
 * @param   options       options for highlighting
 * @param   tsa           template strings array from the template literal
 * @param   keys          any keys passed in the template literal
 *
 * @return  highlighted   string with respect to options passed
 */
export default async function highlight(
  options: HighlightOptions,
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  let concatStr = "";
  for (let i = 0; i < tsa.length; i++) {
    concatStr += tsa[i];
    if (keys[i]) concatStr += keys[i];
  }

  // Provide foreground color
  if (options.color !== Color.NONE) {
    const colorEscapeCode = await getTputEscapeCode(`setaf ${options.color}`);
    concatStr = `${colorEscapeCode}${concatStr}`;
  }

  // Provide background color
  if (options.background !== Color.NONE) {
    const bgColorEscapeCode = await getTputEscapeCode(
      `setab ${options.background}`,
    );
    concatStr = `${bgColorEscapeCode}${concatStr}`;
  }

  // Provide formatting
  if (options.formatting.length !== 0) {
    const { formatting } = options;
    let escapecodes = "";
    for (let i = 0; i < formatting.length; i++) {
      const escapecode = await getTputEscapeCode(formatting[i]);
      escapecodes += escapecode;
    }
    concatStr = `${escapecodes}${concatStr}`;
  }

  // Reset at the end if there were applied options
  if (
    options.color !== Color.NONE ||
    options.background !== Color.NONE ||
    options.formatting.length !== 0
  ) {
    const resetEscapeCode = await getTputEscapeCode("sgr0");
    concatStr += resetEscapeCode;
  }

  return concatStr;
}
