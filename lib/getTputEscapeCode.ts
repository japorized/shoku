/**
 * @function get escape code from tput
 * @param arg command string
 *
 * @returns escape code for command from tput (promisified)
 */
const getTputEscapeCode = async (arg: string) => {
  const p = Deno.run({
    cmd: ["tput", ...arg.split(" ")],
    stdout: "piped",
  });

  const { code } = await p.status();
  let output: string;
  if (code === 0) {
    const rawOutput = await p.output();
    output = new TextDecoder().decode(rawOutput);
  } else output = "";

  p.close();
  return Promise.resolve(output);
};

export default getTputEscapeCode;
