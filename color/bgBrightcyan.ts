import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgBrightcyan — a template literal tag to output text with bright cyan background in the terminal
 *
 * @returns string with escape code for text with bright cyan background in terminal
 *
 * @example
 *   await bgBrightcyan`I am text with bright cyan background!`;
 */
export default function bgBrightcyan(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.BRIGHTCYAN, formatting: [] },
    tsa,
    ...keys,
  );
}
