import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function cyan — a template literal tag to output cyan in the terminal
 *
 * @returns string with escape code for cyan text in terminal
 *
 * @example
 *   await cyan`I am cyan!`;
 */
export default function cyan(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.CYAN, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
