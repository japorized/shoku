import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function blue — a template literal tag to output blue in the terminal
 *
 * @returns string with escape code for blue text in terminal
 *
 * @example
 *   await blue`I am blue!`;
 */
export default function blue(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.BLUE, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
