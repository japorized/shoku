import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function brightgreen — a template literal tag to output bright green in the terminal
 *
 * @returns string with escape code for bright green text in terminal
 *
 * @example
 *   await brightgreen`I am bright green!`;
 */
export default function brightgreen(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.BRIGHTGREEN, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
