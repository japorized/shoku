import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgGray — a template literal tag to output text with gray background in the terminal
 *
 * @returns string with escape code for text with gray background in terminal
 *
 * @example
 *   await bgGray`I am text with gray background!`;
 */
export default function bgGray(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.GRAY, formatting: [] },
    tsa,
    ...keys,
  );
}
