import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgDarkgrey — a template literal tag to output text with dark grey background in the terminal
 *
 * @returns string with escape code for text with dark grey background in terminal
 *
 * @example
 *   await bgDarkgrey`I am text with dark grey background!`;
 */
export default function bgDarkgrey(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.DARKGRAY, formatting: [] },
    tsa,
    ...keys,
  );
}
