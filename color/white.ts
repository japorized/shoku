import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function white — a template literal tag to output white in the terminal
 *
 * @returns string with escape code for white text in terminal
 *
 * @example
 *   await white`I am white!`;
 */
export default function white(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.WHITE, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
