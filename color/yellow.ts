import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function yellow — a template literal tag to output yellow in the terminal
 *
 * @returns string with escape code for yellow text in terminal
 *
 * @example
 *   await yellow`I am yellow!`;
 */
export default function yellow(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.YELLOW, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
