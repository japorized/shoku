import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function darkgray — a template literal tag to output dark gray in the terminal
 *
 * @returns string with escape code for dark gray text in terminal
 *
 * @example
 *   await darkgray`I am dark gray!`;
 */
export default function darkgray(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.GRAY, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
