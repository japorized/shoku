import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgYellow — a template literal tag to output text with yellow background in the terminal
 *
 * @returns string with escape code for text with yellow background in terminal
 *
 * @example
 *   await bgYellow`I am text with yellow background!`;
 */
export default function bgYellow(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.YELLOW, formatting: [] },
    tsa,
    ...keys,
  );
}
