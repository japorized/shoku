import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function grey — a template literal tag to output grey in the terminal
 *
 * @returns string with escape code for grey text in terminal
 *
 * @example
 *   await grey`I am grey!`;
 */
export default function grey(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.GRAY, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
