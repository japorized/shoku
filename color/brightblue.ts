import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function brightblue — a template literal tag to output bright blue in the terminal
 *
 * @returns string with escape code for bright blue text in terminal
 *
 * @example
 *   await brightblue`I am bright blue!`;
 */
export default function brightblue(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.BRIGHTBLUE, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
