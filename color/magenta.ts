import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function magenta — a template literal tag to output magenta in the terminal
 *
 * @returns string with escape code for magenta text in terminal
 *
 * @example
 *   await magenta`I am magenta!`;
 */
export default function magenta(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.MAGENTA, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
