import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function brightred — a template literal tag to output bright red in the terminal
 *
 * @returns string with escape code for bright red text in terminal
 *
 * @example
 *   await brightred`I am bright red!`;
 */
export default function brightred(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.BRIGHTRED, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
