import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgBrightyellow — a template literal tag to output text with bright yellow background in the terminal
 *
 * @returns string with escape code for text with bright yellow background in terminal
 *
 * @example
 *   await bgBrightyellow`I am text with bright yellow background!`;
 */
export default function bgBrightyellow(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.BRIGHTYELLOW, formatting: [] },
    tsa,
    ...keys,
  );
}
