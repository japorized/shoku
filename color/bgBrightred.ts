import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgBrightred — a template literal tag to output text with bright red background in the terminal
 *
 * @returns string with escape code for text with bright red background in terminal
 *
 * @example
 *   await bgBrightred`I am text with bright red background!`;
 */
export default function bgBrightred(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.BRIGHTRED, formatting: [] },
    tsa,
    ...keys,
  );
}
