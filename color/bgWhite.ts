import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgWhite — a template literal tag to output text with white background in the terminal
 *
 * @returns string with escape code for text with white background in terminal
 *
 * @example
 *   await bgWhite`I am text with white background!`;
 */
export default function bgWhite(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.WHITE, formatting: [] },
    tsa,
    ...keys,
  );
}
