import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgBrightgreen — a template literal tag to output text with bright green background in the terminal
 *
 * @returns string with escape code for text with bright green background in terminal
 *
 * @example
 *   await bgBrightgreen`I am text with bright green background!`;
 */
export default function bgBrightgreen(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.BRIGHTGREEN, formatting: [] },
    tsa,
    ...keys,
  );
}
