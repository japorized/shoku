import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function darkgrey — a template literal tag to output dark grey in the terminal
 *
 * @returns string with escape code for dark grey text in terminal
 *
 * @example
 *   await darkgrey`I am dark grey!`;
 */
export default function darkgrey(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.GRAY, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
