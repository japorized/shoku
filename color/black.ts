import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function black — a template literal tag to output black in the terminal
 *
 * @returns string with escape code for black text in terminal
 *
 * @example
 *   await black`I am black!`;
 */
export default function black(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.BLACK, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
