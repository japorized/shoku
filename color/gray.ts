import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function gray — a template literal tag to output gray in the terminal
 *
 * @returns string with escape code for gray text in terminal
 *
 * @example
 *   await gray`I am gray!`;
 */
export default function gray(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.GRAY, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
