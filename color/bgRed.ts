import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgRed — a template literal tag to output text with red background in the terminal
 *
 * @returns string with escape code for text with red background in terminal
 *
 * @example
 *   await bgRed`I am text with red background!`;
 */
export default function bgRed(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.RED, formatting: [] },
    tsa,
    ...keys,
  );
}
