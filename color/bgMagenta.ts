import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgMagenta — a template literal tag to output text with magenta background in the terminal
 *
 * @returns string with escape code for text with magenta background in terminal
 *
 * @example
 *   await bgMagenta`I am text with magenta background!`;
 */
export default function bgMagenta(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.MAGENTA, formatting: [] },
    tsa,
    ...keys,
  );
}
