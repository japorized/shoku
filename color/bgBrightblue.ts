import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgBrightblue — a template literal tag to output text with bright blue background in the terminal
 *
 * @returns string with escape code for text with bright blue background in terminal
 *
 * @example
 *   await bgBrightblue`I am text with bright blue background!`;
 */
export default function bgBrightblue(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.BRIGHTBLUE, formatting: [] },
    tsa,
    ...keys,
  );
}
