import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgGreen — a template literal tag to output text with green background in the terminal
 *
 * @returns string with escape code for text with green background in terminal
 *
 * @example
 *   await bgGreen`I am text with green background!`;
 */
export default function bgGreen(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.GREEN, formatting: [] },
    tsa,
    ...keys,
  );
}
