import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function red — a template literal tag to output red in the terminal
 *
 * @returns string with escape code for red text in terminal
 *
 * @example
 *   await red`I am red!`;
 */
export default function red(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.RED, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
