import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgBlue — a template literal tag to output text with blue background in the terminal
 *
 * @returns string with escape code for text with blue background in terminal
 *
 * @example
 *   await bgBlue`I am text with blue background!`;
 */
export default function bgBlue(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.BLUE, formatting: [] },
    tsa,
    ...keys,
  );
}
