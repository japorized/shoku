import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgBlack — a template literal tag to output text with black background in the terminal
 *
 * @returns string with escape code for text with black background in terminal
 *
 * @example
 *   await bgBlack`I am text with black background!`;
 */
export default function bgBlack(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.BLACK, formatting: [] },
    tsa,
    ...keys,
  );
}
