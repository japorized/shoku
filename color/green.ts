import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function green — a template literal tag to output green in the terminal
 *
 * @returns string with escape code for green text in terminal
 *
 * @example
 *   await green`I am green!`;
 */
export default function green(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.GREEN, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
