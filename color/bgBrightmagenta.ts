import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgBrightmagenta — a template literal tag to output text with bright magenta background in the terminal
 *
 * @returns string with escape code for text with bright magenta background in terminal
 *
 * @example
 *   await bgBrightmagenta`I am text with bright magenta background!`;
 */
export default function bgBrightmagenta(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.BRIGHTMAGENTA, formatting: [] },
    tsa,
    ...keys,
  );
}
