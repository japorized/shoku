import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgDarkgray — a template literal tag to output text with dark gray background in the terminal
 *
 * @returns string with escape code for text with dark gray background in terminal
 *
 * @example
 *   await bgDarkgray`I am text with dark gray background!`;
 */
export default function bgDarkgray(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.DARKGRAY, formatting: [] },
    tsa,
    ...keys,
  );
}
