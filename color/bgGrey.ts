import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgGrey — a template literal tag to output text with grey background in the terminal
 *
 * @returns string with escape code for text with grey background in terminal
 *
 * @example
 *   await bgGrey`I am text with grey background!`;
 */
export default function bgGrey(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.GRAY, formatting: [] },
    tsa,
    ...keys,
  );
}
