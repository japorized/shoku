import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function brightyellow — a template literal tag to output bright yellow in the terminal
 *
 * @returns string with escape code for bright yellow text in terminal
 *
 * @example
 *   await brightyellow`I am bright yellow!`;
 */
export default function brightyellow(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.BRIGHTYELLOW, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
