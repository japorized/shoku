import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function brightmagenta — a template literal tag to output bright magenta in the terminal
 *
 * @returns string with escape code for bright magenta text in terminal
 *
 * @example
 *   await brightmagenta`I am bright magenta!`;
 */
export default function brightmagenta(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.BRIGHTMAGENTA, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
