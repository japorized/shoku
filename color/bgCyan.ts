import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function bgCyan — a template literal tag to output text with cyan background in the terminal
 *
 * @returns string with escape code for text with cyan background in terminal
 *
 * @example
 *   await bgCyan`I am text with cyan background!`;
 */
export default function bgCyan(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.NONE, background: Color.CYAN, formatting: [] },
    tsa,
    ...keys,
  );
}
