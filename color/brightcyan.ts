import highlight from "../lib/highlight.ts";
import { Color, Stringable } from "../lib/types.ts";

/**
 * @function brightcyan — a template literal tag to output bright cyan in the terminal
 *
 * @returns string with escape code for bright cyan text in terminal
 *
 * @example
 *   await brightcyan`I am bright cyan!`;
 */
export default function brightcyan(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    { color: Color.BRIGHTCYAN, background: Color.NONE, formatting: [] },
    tsa,
    ...keys,
  );
}
