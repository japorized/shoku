import { assertStringContains } from "https://deno.land/std@0.69.0/testing/asserts.ts";
import { Stringable, Color, Formatting } from "./lib/types.ts";
import highlight from "./lib/highlight.ts";
import getTputEscapeCode from "./lib/getTputEscapeCode.ts";

Deno.test("Advanced use cases", async () => {
  function myHighlight(
    tsa: TemplateStringsArray,
    ...keys: Stringable[]
  ): Promise<string> {
    return highlight(
      {
        color: Color.BLACK,
        background: Color.CYAN,
        formatting: [Formatting.BOLD, Formatting.UNDERLINE],
      },
      tsa,
      ...keys,
    );
  }
  const output = await myHighlight` Test me! `;
  const sgr0 = await getTputEscapeCode("sgr0");
  assertStringContains(output, sgr0);
});
