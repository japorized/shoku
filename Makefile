.POSIX:

init:
	@echo "Setting up git hooks..."
	git config core.hooksPath .githooks
	@echo "Setup complete! Code away!"

lint:
	@echo " Linting..."
	@deno lint --unstable

fmt:
	@echo " Checking formatting..."
	@deno fmt --check

fmt-write:
	@echo " Overwriting formatting"
	@deno fmt

test: fmt lint
	@echo " Running tests..."
	@deno test --allow-run test.ts
