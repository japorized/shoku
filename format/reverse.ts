import highlight from "../lib/highlight.ts";
import { Color, Formatting, Stringable } from "../lib/types.ts";

/**
 * @function reverse — a template literal tag to output reversed coloring in the terminal
 *
 * @returns string with escape code for reversed coloring text in terminal
 *
 * @example
 *   await reverse`I am reversed!`;
 */
export default function reverse(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    {
      color: Color.NONE,
      background: Color.NONE,
      formatting: [Formatting.REVERSE],
    },
    tsa,
    ...keys,
  );
}
