import highlight from "../lib/highlight.ts";
import { Color, Formatting, Stringable } from "../lib/types.ts";

/**
 * @function dim — a template literal tag to output dimmed text in the terminal
 *
 * @returns string with escape code for dimmed text in terminal
 *
 * @example
 *   await dim`I am dimmed!`;
 */
export default function dim(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    {
      color: Color.NONE,
      background: Color.NONE,
      formatting: [Formatting.DIM],
    },
    tsa,
    ...keys,
  );
}
