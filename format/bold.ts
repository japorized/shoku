import highlight from "../lib/highlight.ts";
import { Color, Formatting, Stringable } from "../lib/types.ts";

/**
 * @function bold — a template literal tag to output bolded text in the terminal
 *
 * @returns string with escape code for bolded text in terminal
 *
 * @example
 *   await bold`I am bolded!`;
 */
export default function bold(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    {
      color: Color.NONE,
      background: Color.NONE,
      formatting: [Formatting.BOLD],
    },
    tsa,
    ...keys,
  );
}
