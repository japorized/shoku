import highlight from "../lib/highlight.ts";
import { Color, Formatting, Stringable } from "../lib/types.ts";

/**
 * @function underline — a template literal tag to output underlined text in the terminal
 *
 * @returns string with escape code for underlined text in terminal
 *
 * @example
 *   await underline`I am underlined!`;
 */
export default function underline(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    {
      color: Color.NONE,
      background: Color.NONE,
      formatting: [Formatting.UNDERLINE],
    },
    tsa,
    ...keys,
  );
}
