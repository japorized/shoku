#!/bin/sh

rst=$(tput sgr0)
red=$(tput setaf 1)
grn=$(tput setaf 2)
mgt=$(tput setaf 5)

label_msg="pre-commit hook"
label="[${mgt}$label_msg${rst}]"
label_bad="[${red}$label_msg${rst}]"
label_good="[${grn}$label_msg${rst}]"

## Pre-commit formatting
FILES=$(git diff --cached --name-only --diff-filter=ACMR "*.ts" | sed 's| |\\ |g')
[ -z "$FILES" ] && exit 0

# Prettify all selected files
echo "$label Running deno fmt on staged files..."
echo "$FILES" | xargs deno fmt

# Add back the modified/prettified files to staging
echo "$FILES" | xargs git add

# Lint staged files
echo "$label Running deno lint --unstable on staged files..."
echo "$FILES" | xargs deno lint --unstable
retval=$?
if [ ! $retval -eq 0 ]; then
  echo "$label_bad Lint check failed ❌ Aborting commit..."
  exit $retval
fi

echo "$label_good Pre-commit hook passed ✔"
exit 0
