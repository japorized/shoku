# Shoku

Terminal colors and formatting for Deno cli applications

[![Build Status](https://img.shields.io/gitlab/pipeline/japorized/shoku/master.svg?style=for-the-badge)](https://gitlab.com/japorized/shoku/-/pipelines)

---

## Usage

**System dependency**: `ncurses (tput)`

```typescript
// index.ts

import { red } from "https://gitlab.com/japorized/shoku/-/raw/master/mod.ts";
// or for a slightly leaner import (note the default import)
// import red from "https://gitlab.com/japorized/shoku/-/raw/master/colors/red.ts"

console.log(await red`Shoku`);
```

```bash
deno run --allow-run index.ts
```

For more complicated usage, e.g. bold, underlined, black text with cyan background,
while it's possible to do the following:

```typescript
console.log(await bgCyan`${await black`${await underline`${await bold`Hello!`}`}`}`);
```

it's not recommended, and I would propose writing your own template literal tag for it:

```typescript
import highlight from "https://gitlab.com/japorized/shoku/-/raw/master/lib/highlight.ts";
import { Color, Formatting } from "https://gitlab.com/japorized/shoku/-/raw/master/lib/types.ts";

function myHighlight(tsa: TemplateStringsArray, ...keys: any[]): Promise<string> {
  return highlight(
    {
      color: Color.BLACK,
      background: Color.CYAN,
      formatting: [Formatting.BOLD, Formatting.UNDERLINE]
    },
    tsa,
    ...keys,
  );
}
```

---

## Development

### Setup

```
git clone git@gitlab.com:japorized/shoku.git
cd shoku
make init
```

### Formatting

```
make format         # checks formatting with deno fmt --check
make format-write   # writes formatting with deno fmt
```

### Tests

```
make test
```

---

## Limitations

In order to preserve cross-platform compatibility, the library uses `tput`,
a command provided by the `ncurses` library to grab terminal compatible escape
code for the colors and formatting. As a result, [`Deno.run`](https://doc.deno.land/https/github.com/denoland/deno/releases/latest/download/lib.deno.d.ts#Deno.run)
is required, which thus requires the `--allow-run` flag to be set.

Also as a result of using `Deno.run`, the process of getting feedback from `tput`
is async, and so the `await` feature is required while using any of the colors or
formatting.
