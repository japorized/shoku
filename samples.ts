import {
  black,
  red,
  green,
  yellow,
  gray,
  cyan,
  bgRed,
  bgYellow,
  bold,
  dim,
  reverse,
  underline,
} from "./mod.ts";
import highlight from "./lib/highlight.ts";
import { Color, Formatting, Stringable } from "./lib/types.ts";

const name = "John Smith";
const age = 21;

console.log(await gray`Hello there ${name}! ${age} is a fine age.`);
console.log(`${await red`${await bold`User`}`}: ${name}
${await yellow`Age`}: ${age}`);

console.log("\nOther tests:");
console.log(await green`Green`);
console.log(await bold`Bold`);
console.log(await dim`Dim`);
console.log(await red`${await dim`Dimmed red`}`);
console.log(await reverse`Reverse`);
console.log(await cyan`${await reverse`Reverse cyan`}`);
console.log(await underline`Underline`);
console.log(await bgRed`Red background`);

console.log("\nAdvanced uses:");
function myHighlight(
  tsa: TemplateStringsArray,
  ...keys: Stringable[]
): Promise<string> {
  return highlight(
    {
      color: Color.BLACK,
      background: Color.CYAN,
      formatting: [Formatting.BOLD, Formatting.UNDERLINE],
    },
    tsa,
    ...keys,
  );
}
console.log(await myHighlight` Test me! `);

console.log("\nProblems:");
console.log(
  await bgYellow` ${await black
    `I should have 2 spaces with yellow background on both sides`} `,
);
console.log(
  await bgYellow` ${await black
    `Workaround: put space before the first reset `}`,
);
